
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , sio = require('socket.io')
  , path = require('path');

var app = express()
  , http = require('http')
  
  


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));



app.get('/room/*', function(req, res){
	res.render('index', { title: 'The index page!' })
  console.log(req.url);
});


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

var server = http.createServer(app);
io = require('socket.io').listen(server);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var users = [];
function establishRoom(roomName){
if(!users[roomName]){
 	users[roomName] = [];
 }
}


var messages = [];
var raisedFingers =[];

// Define a message handler
io.sockets.on('connection', function (socket) {
 
socket.on('subscribe', function(data) { 
	
	establishRoom('/'+data.room);
	socket.join(data.room); 
	 socket.emit('users',users[io.sockets.manager.roomClients[socket.id][0]]);
})

socket.on('unsubscribe', function(data) { socket.leave(data.room); })

 socket.on('arrived', function(user) {
 	establishRoom(io.sockets.manager.roomClients[socket.id][0])
 	 users[io.sockets.manager.roomClients[socket.id][0]].push(user);
 	 socket.broadcast.emit('arrival',user);
 });
  socket.on('raisefinger', function(user){
    console.log('got a raised finger');
    raisedFingers.push(user);
    socket.broadcast.emit('raisedfinger',user)
  });
  socket.on('passmonkey',function(data){
  	console.log('Passed monkey to '+data.name+' with id '+data.id);
  	socket.broadcast.emit('changespeaker',data)
  });

  socket.on('message', function (msg) {
    console.log('Received: ', msg);
    messages.push(msg);
    socket.broadcast.emit('message', msg);
  });
  // send messages to new clients
 
   

});








